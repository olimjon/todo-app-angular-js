var todoApp = angular.module('todoApp', ['ui.sortable', 'ngMaterial']);
	todoApp.config(function($mdIconProvider) {
		$mdIconProvider
				.defaultFontSet('FontAwesome')
				.fontSet('fa', 'FontAwesome');
	});
	todoApp.controller('headerController', ['$scope', function($page){
		$page.title = 'TODO Application';
	}]);
	todoApp.controller('todoController', function($scope, $timeout, $mdBottomSheet, $mdToast, $mdDialog){
		$scope.appName = 'ToDo App';
		$scope.refresh = function(){
			$scope.todoList.forEach(function(e){
					e.tasks = e.tasks.filter(x => x.status === false);
			});
		};
		
		$scope.todoList = [
			{
				category: 'Inbox',
				tasks: [
					{
						title: 'New task 1',
						date: false,
						status: false
					}
				]
			},
			{
				category: 'Project A',
				tasks: [
					{
						title: 'Finish backlog 1',
						date: false,
						status: false
					},
					{
						title: 'Finish backlog 2',
						date: false,
						status: true
					},
					{
						title: 'Finish backlog 3',
						date: false,
						status: false
					}
				]
			},
			{
				category: 'Project B',
				tasks: [
					{
						title: 'Finish backlog 1',
						date: false,
						status: false
					},
					{
						title: 'Finish backlog 2',
						date: false,
						status: false
					}
				]
			}
		];
		$scope.sortableOptions = {
			'ui-floating': false,
			axis: 'y',
			placeholder: 'item',
			connectWith: 'apps-container',
            update: function(event, ui){
            	console.log(event);
            	console.log(ui);
            	console.log($scope.todoList);
            }
		};
		$scope.showInputPrompt = function() {
			$scope.alert = '';
			$mdBottomSheet.show({
				templateUrl: 'input-template',
				controller: 'InputBoxPromptControl',
				clickOutsideToClose : true,
				locals: {todoList: $scope.todoList}
			}).then(function(task) {
				$scope.todoList[$scope.todoList.findIndex(x => x.category === task.category.name)].tasks.push(task.newTask);
			}).catch(function(error) {
				console.log(error);
			});
		};

		$scope.addCategory = function(ev) {
			var confirm = $mdDialog.prompt()
				.title('Please add new TODO category')
				.placeholder('Category name')
				.ariaLabel('Category name')
				.initialValue('')
				.targetEvent(ev)
				.required(true)
				.ok('Add')
				.cancel('Cancel');

			$mdDialog.show(confirm).then(function(newCategory) {
				$scope.todoList.push({
					category: newCategory,
					tasks: []
				});
			}, function() {
				
			});
		};

	});
	todoApp.controller('InputBoxPromptControl', function($scope, $timeout, $q, $mdBottomSheet, todoList){
		$scope.categories = todoList;
		$scope.today = new Date();
		var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		$scope.add = function(){
			if(($scope.task != undefined || '')){
				$mdBottomSheet.hide(
					{
						category: {
							name: $scope.category || 'Inbox'
						},
						newTask: {
							title: $scope.task,
							date: new Date($scope.taskDate) != 'Invalid Date' ? ($scope.taskDate.getDate() + '-' + months[$scope.taskDate.getMonth()]) : false || false,
							status: false
						}
					}
				);
			}
		};
	});
	todoApp.directive('pressEnter', function () {
	    return function (scope, element, attrs) {
	        element.bind("keydown keypress", function (event) {
	            if(event.which === 13) {
	                scope.$apply(function (){
	                    scope.$eval(attrs.pressEnter);
	                });

	                event.preventDefault();
	            }
	        });
	    };
	});